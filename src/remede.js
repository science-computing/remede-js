import getCookie from './utilities/getcookie'

const R = window.R = require('ramda');
const m = window.m = require('mithril');
const Stream = window.Stream = require('mithril/stream');

const d = window.d = {
  csrfrequest: R.compose(
    m.request,
    R.mergeDeepRight(
      {
        headers: {
          'X-CSRFToken': getCookie('csrftoken')
        },
      }
    )
  )
}

export {R, m, Stream, d}
