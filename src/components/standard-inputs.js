// All the fancy inputs
// Should handle Text, Select, Numbers, Checkbox, Radios, Multiselect, Color?
import * as R from 'ramda'
import m from 'mithril'


function TextInput (vn) {
  var oninput = function(evt){
    vn.attrs.oninput(evt.target.value)
  }
  return {
    view: function(vn){
      return m('input', R.mergeDeepLeft(
        {oninput: m.withAttr("value", vn.attrs.oninput)}, vn.attrs
      ))
    }
  }
}

function SelectInput (vn) {
  var oninput = function(evt){
    vn.attrs.oninput(evt.target.value)
  }
  return {
    view: function(vn){
      return m('select',
        {
          oninput: m.withAttr("value", vn.attrs.oninput),
          value: vn.attrs.value,
          class: vn.attrs.class,
        },
        vn.attrs.options.map(function(option){
          return m('option', {value: option.value}, option.label)
        })
      )
    }
  }
}



export {
  TextInput,
  SelectInput,
}
