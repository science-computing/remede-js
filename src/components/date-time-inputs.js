import {R, m} from '../remede'

// Inline Styling constants because we're horrible people
var day_bubble_style = {
  'cursor': 'pointer',
  'margin': '5px',
  'padding': '0 5px 0 5px',
  'display': 'block',
  'font-size': '10px',
  'line-height': '20px',
  'text-align': 'center',
  'border-radius': '12px',
}
var month_bubble_style = {
  'cursor': 'pointer',
  'margin': '5px',
  'padding': '0 10px 0 10px',
  'display': 'block',
  'font-size': '10px',
  'line-height': '20px',
  'text-align': 'center',
  'border-radius': '12px',
}
var year_bubble_style = {
  'margin': '2px',
  'background-color': '#ddd',
  'padding': '0 10px 0 10px',
  'font-weight': 'bold',
  'font-size': '13px',
  'line-height': '16px',
  'text-align': 'center',
  'border-radius': '12px',
  'cursor': 'pointer',
}

// Determines style for selected vs unselected bubble
var genBubbleStyle = function(base, bool){
  return R.assoc('background-color', bool ? '#b8b8ff' : '#ddd', base)
}

function DateSelection(vn){
  function getDays(month, year) {
     var date = new Date(parseInt(year), parseInt(month), 1);
     date.getMonth() == parseInt(month)
     var days = [];
     while (date.getMonth() === parseInt(month)) {
        days.push(new Date(date).getDate());
        date.setDate(date.getDate() + 1);
     }
     
     console.log(days)
  }

  var onchange = function(val, close){
    if (val === null){
      val = ''
    }
    else{
      getDays(val.slice(0,4), val.slice(5,7))
    }
    vn.attrs.onchange(val, close)
  }
  var clickmonth = function(date, val, close){
    return function(_evt){
      onchange(
        date.slice(0,5) + 
        val.toString().padStart(2, '0') +
        date.slice(7, 10),
        close
      )
    }
  }
  var clickclear = function(_evt){
    onchange('', true)
  }
  var clickday = function(date, val, close){
    return function(_evt){
      onchange(
        date.slice(0,8) + 
        val.toString().padStart(2, '0'),
        close
      )
    }
  }
  var shiftYear = function(date, val){
    return function(_evt){
      onchange( 
        parseInt(date.slice(0, 4)) + val +
        date.slice(4)
      )
    }
  }
  
  var months = [
    'jan', 'feb', 'mar',
    'apr', 'may', 'jun',
    'jul', 'aug', 'sep',
    'oct', 'nov', 'dec',
  ]
  var days = new Array(31).fill().map(function(x, i){
    return (i+1).toString().padStart(2, '0')
  })
  return {
    days: [],
    oncreate: function(vn){
      if (
        vn.attrs.date === undefined ||
        vn.attrs.date.length !== 10
      ){
        var date = new Date()
          .toISOString().slice(0,10)
        onchange(date)
        m.redraw()
      }
    },
    view: function(vn){
      if (
        vn.attrs.date === undefined ||
        vn.attrs.date.length !== 10
      ){
        return null
      }
      var year = vn.attrs.date.slice(0, 4)
      var month = parseInt(vn.attrs.date.slice(5, 7))
      var day = parseInt(vn.attrs.date.slice(8, 10))
      return m('', [
        m('', {style: 'display: flex; padding: 10px;'}, [
          m(
            'pre',
            {
              style: year_bubble_style,
              onclick: shiftYear(vn.attrs.date, -10)
            },
            m.trust('&laquo;')
          ),
          m(
            'pre',
            {
              style: year_bubble_style,
              onclick: shiftYear(vn.attrs.date, -1)
            },
            m.trust('&lsaquo;')
          ),
          m('pre', {style: 'flex-grow: 1; text-align: center; margin: 2px;'}, year),
          m(
            'pre',
            {
              style: year_bubble_style,
              onclick: shiftYear(vn.attrs.date, 1)
            },
            m.trust('&rsaquo;')
          ),
          m(
            'pre',
            {
              style: year_bubble_style,
              onclick: shiftYear(vn.attrs.date, 10)
            },
            m.trust('&raquo;')
          ),
        ]),
        m('', {style: 'display: flex; flex-wrap: wrap; justify-content: center;'}, months.map(function(x, i){
          return m('pre', {
            style: genBubbleStyle(month_bubble_style, month==i+1),
            onclick: clickmonth(vn.attrs.date, i+1),
            ondblclick: clickmonth(vn.attrs.date, i+1, true)
          }, x)
        })),
        m('', {style: 'display: flex; flex-wrap: wrap; justify-content: center;'}, days.map(function(x, i){
          return m('', {
            style: genBubbleStyle(day_bubble_style, day==i+1),
            onclick: clickday(vn.attrs.date, i+1),
            ondblclick: clickday(vn.attrs.date, i+1, true)
          }, x)
        })),
        m('div', {
            style: genBubbleStyle(month_bubble_style, false),
            onclick: clickclear,
        }, 'Cancel / Clear')
      ])
    }
  }
}


function DateInput(vn){
  var tick = typeof(vn.attrs.tick) === 'function' ? vn.attrs.tick : function(){}
  var toggleShow = function(){
    vn.state.show = !vn.state.show
    if (!vn.state.show){
    }
  }
  var oninput = function(val, close){
    // refocus so we can hit enter
    vn.attrs.oninput(val)
    // Trigger onchange. This is a bit much, but works...
    if (val !== vn.attrs.value) vn.dom.onchange()
    vn.dom.focus()
    if(close){
      toggleShow()
    }
  }
  var getInputLocation = function(){
    var rect = vn.dom.getBoundingClientRect()
    vn.state.inputLocation = {x: rect.x, y:rect.y}
    tick()
  }
  return {
    show: false,
    inputLocation: {x:0, y:0},
    view: function(vn){
      return [
        m('input', R.mergeDeepLeft({
          autocomplete: 'off',
          onfocus: function(){
            if (!vn.state.show){
              toggleShow()
            }
            getInputLocation()
            // Watch it...
            document.addEventListener(
              'scroll',
              getInputLocation
            )
          },
          onkeypress: function(){
            return false
          },
          onkeydown: function(evt){
            if (evt.key == 'Escape' || evt.key == 'Enter'){
              toggleShow()
              vn.dom.blur()
            }
            if (evt.key == 'Tab'){
              return true
            }
            return false
          },
          // We're forcefully blocking the natural oninput
          oninput: function(){},
          onblur: function(evt){
            
            if (evt.relatedTarget !== null && evt.relatedTarget.classList.contains('MITHRIL-DATEPICK-POPUP') ){
            } else {
              vn.state.show = false
            }
            document.removeEventListener(
              'scroll',
              getInputLocation(),
            )
            tick()
          },
        }, vn.attrs)),
        !vn.state.show ? null :
        m('.MITHRIL-DATEPICK-POPUP', {
            tabindex: -1,
            onclick: function(){
              vn.dom.focus()
            },
            style: {
              'z-index': 50,
              'outline': 'none',
              'width': '200px',
              'border': '1px solid pink',
              'position': 'fixed',
              'background-color': 'white',
              'left': vn.state.inputLocation.x + 'px',
              'top': vn.state.inputLocation.y + 'px',
            }
          }, [
          m(DateSelection, {
            date: vn.attrs.value,
            onchange: oninput,
          })
        ])
      ]
    }
  }
}


export {
  DateInput
}
