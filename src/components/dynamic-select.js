import {R, m} from '../remede'
import debounce from '../utilities/debounce'

function DynamicSelectOption(vn){
	return {
  	view: function(vn){
    	return m('', {
      	style: {
        	'width': '100%',
          'cursor': 'pointer',
         },
      	onmousedown: function(_evt){
        	vn.attrs.handleSelect(
          	vn.attrs.accessor(vn.attrs.option)
          )
        }
      }, vn.attrs.display(vn.attrs.option))
    }
  }
}

function DynamicSelectInput(vn){
	var tick = typeof(vn.attrs.tick) === 'function' ? vn.attrs.tick : function(){m.redraw()}
  var getOptions = function(val){
    vn.attrs.getOptions(
      val,
      function(options){
        vn.state.options = options
        tick()
      }
    )
  }
  var debouncedGetOptions = debounce(getOptions, 400)
	var oninput = function(evt){
  	vn.attrs.oninput(evt.target.value)
   	debouncedGetOptions(evt.target.value)
  }
  var onblur = function(_evt){
  	vn.state.show = false
    vn.state.options = []
    tick()
  }
  var handleSelect = function(val){
  	vn.attrs.oninput(val)
    onblur()
  }
  // THis is a really lazy way to do this
  // TODO figure out proper debouncing for the scrolling
  var updateRect = debounce(function(d){
  	vn.state.inputrect = d.getBoundingClientRect()
    tick()
  }, 100)
	return {
  	show: false,
    inputrect: new DOMRect(),
    options: [],
    view: function(vn){
    	return m('', [
      	m('input', R.mergeDeepLeft({
          autocomplete: 'off',
        	oninput: oninput,
          onfocus: function(){
          	updateRect(this)
          	getOptions(vn.attrs.value)
          	vn.state.show = true;
            var input = this
            document.addEventListener(
              'scroll',
              function(){
                updateRect(input)
              }
            )
          },
          onblur: function(){
          	// Defer till end of stack (let click happen first)
          	setTimeout(onblur, 10)
          },
          value: vn.attrs.value
        }, vn.attrs)),
        !vn.state.show ? null :
        m('',
        	{style: {
          	'width': vn.state.inputrect.width + 'px',
            'padding': '2px',
            'border': '1px solid #ddd',
            'box-sizing': 'border-box',
            'overflow-x': 'hidden',
            'position': 'fixed',
            'background-color': 'white',
            'z-index': 10,
            'left': vn.state.inputrect.left + 'px',
            'top': vn.state.inputrect.bottom + 'px'
          }},
        	R.map(function(x){
          	return m(DynamicSelectOption, {
            	option: x,
              handleSelect: handleSelect,
              accessor: R.defaultTo(R.identity, vn.attrs.accessor),
              display: R.defaultTo(R.identity, vn.attrs.display),
            })
          }, vn.state.options)
        )
      ])
    }
  
  }
}


export default DynamicSelectInput
