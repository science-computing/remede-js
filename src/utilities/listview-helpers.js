import * as R from 'ramda'
import m from 'mithril'


// node -> obj
var parseHeader = function(node){
  var header = {}
  header.title = node.innerText || node.textContent
  header.order = node.getAttribute('data-order')
  return header
}

// node -> obj
var parseFilter = function(node){
  var filter = {}
  var tagName = node.tagName
  filter.filter = node.getAttribute('data-filter')
  filter.label = node.getAttribute('data-label')
  filter.helptext = node.getAttribute('data-helptext')
  if (tagName === 'SELECT'){
    filter.input_type = 'select'
    filter.options = R.compose(
      R.map(function(x){
        var option = {}
        option.label = x.innerText || x.textContent
        option.value = x.getAttribute('value')
        return option
      }),
      Array.from
    )(node.children)
  } else {
    filter.input_type = node.getAttribute('type') || 'text'
  }
  return filter
}

// array -> object | TESTED
var reduceFilters = function(filters){
  return filters.reduce(function(acc, filter){
    var value = filter.value
    var key = filter.filter

    return value === undefined ? acc :
           key === undefined   ? acc :
           R.merge(acc, {[key]: value})
  }, {})
}

// (*, str) -> bool
var paramNotBlank = function(val, key){
  return R.equals(val, []) ? false :
         R.equals(val, '') ? false :
         true
}


export {
  parseHeader,
  parseFilter,
  reduceFilters,
  paramNotBlank,
}
