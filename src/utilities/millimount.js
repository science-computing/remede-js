import {m, Stream} from '../remede'

// A Tiny self-contained version of m.mount
// f component must take an update and return a component
var millimount = function(node, f_component){
  var update_stream = Stream()
  var app = f_component(update_stream)
  var models = Stream.scan(
    function(model, func){
      return func(model)
    },
    app.model(),
    update_stream
  )

  models.map(function(model){
    m.render(node, app.view(model))
  })
  return models
}


export default millimount
