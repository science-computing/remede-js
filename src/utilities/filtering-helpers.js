import * as R from 'ramda'


  // str -> str | 'Hi' -> '-Hi' | TESTED
  var negateString = R.ifElse(
    R.startsWith('-'),
    R.drop(1),
    R.concat('-')
  )

  // str -> array -> int | TESTED
  var getNIndex = R.curry(function(key, arr){
    return R.ifElse(
      R.contains(key),
      R.indexOf(key),
      R.indexOf(negateString(key))
    )(arr)
  })

  // str -> array -> array | TESTED
  var appendOrNegate = R.curry(function(key, arr){
    var index = getNIndex(key)(arr)

    if (index !== -1){
      return R.over(
        R.lensIndex(index),
        negateString,
        arr
      )
    }
    return R.append(key, arr)
  })

  // str -> array -> array | TESTED
  var setOrNegate = R.curry(function(key, arr){
    var index = getNIndex(key)(arr)

    if (index !== -1){
      return [negateString(arr[index])]
    }
    return [key]
  })

  // Functional Helpers -------------------------
  // *|array -> array | TESTED
  var wrapArray = function(obj){
    if (R.type(obj) !== 'Array'){
      return [obj]
    }
    return obj
  }


export {
  negateString,
  getNIndex,
  appendOrNegate,
  setOrNegate,
  wrapArray,
}
