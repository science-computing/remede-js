var attrsToObj = function(node){
  var attrs = node.attributes
  var obj = {}
  for (var a=0; a<attrs.length; a++){
    obj[attrs[a].name] = attrs[a].value
  }
  return obj
}

export default attrsToObj
