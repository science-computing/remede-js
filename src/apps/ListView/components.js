import {R, m} from '../../remede'

import {
  TextInput,
  SelectInput,
} from '../../components/standard-inputs'

import {
  negateString,
  setOrNegate,
  appendOrNegate,
  wrapArray,
} from '../../utilities/filtering-helpers'

import G from './globals'
import app from './state'

// Local utilities...
var asc_icon = ' 	▴'
var desc_icon = ' ▾'
var order_icon = ' ▸'

var getOrderIcon = R.curry(function(key, arr){
  if (R.contains(key, arr)){return m.trust(desc_icon)}
  else if(R.contains(negateString(key), arr)){return m.trust(asc_icon)}
  return m.trust(order_icon)
})


// Components
function HeaderItem (vn) {
  var handleClick = function(evt){
    if (!vn.attrs.order){
      return
    }
    var modifyFunction = setOrNegate(vn.attrs.order)

    if (evt.shiftKey){
      modifyFunction = appendOrNegate(vn.attrs.order)
    }

    // Handle if we already have this item in the order...
    app.ACTIONS.go(
      R.over(
        R.lensProp(G.filtering.orderby_param),
        R.compose(
          modifyFunction,
          wrapArray,
          R.defaultTo([])
        )
      )
    )
  }

  var orderIcon = function(){
    if (!vn.attrs.order) return ''
    return R.compose(
      getOrderIcon(vn.attrs.order),
      R.defaultTo([]),
    )(app.STATE.searchParams[G.filtering.orderby_param])
  }

  return {
    view: function(){
      return m(
        'th',
        {
          onclick: handleClick,
          style: vn.attrs.order ? 'cursor: pointer;' : ''
        },
        [
          vn.attrs.title,
          orderIcon(),
        ]
      )
    }
  }
}



function FilterItem (vn) {
  var chooseInput = function(inputType){
    return inputType === 'select' ? SelectInput :
                         TextInput
  }
  return {
    view: function(vn){
      return m('.MITHRIL-FILTERS-FilterItem',
        {class: G.classes.filters.input_div},
        [
        m('label', vn.attrs.label),
          m(
            chooseInput(vn.attrs.input_type),
            vn.attrs
          ),
          m('div', {class: G.classes.filters.help}, vn.attrs.helptext),
      ])
    }
  }
}

function PaginationItem (vn) {
  var onclick = function(_evt){
    if (vn.attrs.dest){
      app.ACTIONS.go(R.mergeDeepLeft({page: vn.attrs.dest}))
    }
  }
  return {
    view: function(vn){
      return m('li', {class: G.classes.pagination[vn.attrs.dest ? 'link' : 'text']}, [
        m(
          'span',
          {
            onclick: onclick,
            class: G.classes.pagination.item_content,
          },
          vn.children.length > 0 ? vn.children : vn.attrs.dest
        )
      ])
    }
  }
}

export {
  HeaderItem,
  FilterItem,
  PaginationItem,
}
