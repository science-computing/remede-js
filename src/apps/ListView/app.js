import * as R from 'ramda'
import m from 'mithril'

import {
  HeaderItem,
  FilterItem,
  PaginationItem,
} from './components'

import {
  SelectInput,
} from '../../components/standard-inputs'

import {
  reduceFilterItems,
  reduceFilters,
  parseHeader,
  paramNotBlank,
} from '../../utilities/listview-helpers.js'

import G from './globals'
import app from './state'

var Header = {
  // Generate headers from static
  headers: R.compose(
    R.map(parseHeader),
    Array.from
  )(document.querySelectorAll('#MITHRIL-THEAD th')),
  view: function(vn){
    return m('tr', Header.headers.map(
      function(x){
        return m(HeaderItem, x)
      }
    ))
  }
}

var Filters = {
  oninit: function(){
    app.ACTIONS.initFilterValues()
  },
  applyFilters: function(_evt){
    app.ACTIONS.go(R.compose(
      R.pickBy(paramNotBlank),
      R.mergeDeepLeft(reduceFilters(app.STATE.filters)),
      R.mergeDeepLeft({[G.filtering.page_param]: 1})
    ))
  },
  clearFilters: function(_evt){
    app.ACTIONS.go(R.pick([G.filtering.orderby_param, G.filtering.page_param]))
  },
  handleEnterKey: function(evt){
    if (evt.keyCode === 13){
      Filters.applyFilters()
    }
  },
  view: function(vn){
    return m('', {class: G.classes.filters.div}, [
      app.STATE.filters.map(
        function(filter, index){
          return m(FilterItem,
            R.merge(
              filter,
              {
                onkeyup: Filters.handleEnterKey,
                oninput: app.ACTIONS.updateFilter(index),
                class: G.classes.filters.input,
              }
            )
          )
        }
      ),
      m('div', {class: G.classes.filters.button_div}, [
        m('button', {class: G.classes.filters.apply_button, onclick: Filters.applyFilters}, 'Apply'),
        m('button', {class: G.classes.filters.clear_button, onclick: Filters.clearFilters}, 'Clear'),
      ])
    ])
  }
}

var Pagination = {
  view: function(vn){
    return m('div', {class: G.classes.pagination.div}, [
      m(SelectInput, {
        options: G.pagination.paginate_options.map(
          function(option){
            return {value: option, label: 'Show ' + option + ' records per page'}
          }
        ),
        value: G.pagination.paginate_by,
        class: G.classes.pagination.select,
        oninput: function(val){app.ACTIONS.go(R.mergeDeepLeft({paginate_by: val, page: 1}))}
      }),
      m('ul', {class: G.classes.pagination.list}, [
        app.STATE.pagination.prev_page ? [
          m(PaginationItem, {dest: app.STATE.pagination.first_page}, 'first'),
          m(PaginationItem, {dest: app.STATE.pagination.prev_page}, 'prev')
        ] : null,
        m(PaginationItem, 'Page ' + app.STATE.pagination.current_page + ' of ' + app.STATE.pagination.last_page),
        app.STATE.pagination.next_page ? [
          m(PaginationItem, {dest: app.STATE.pagination.next_page}, 'next'),
          m(PaginationItem, {dest: app.STATE.pagination.last_page}, 'last')
        ] : null
      ])
    ])
  }
}

console.log(G)
console.log('hi')
m.mount(document.getElementById('MITHRIL-THEAD'), Header)
m.mount(document.getElementById('MITHRIL-FILTERS-filters'), Filters)
m.mount(document.getElementById('MITHRIL-PAGINATION'), Pagination)
