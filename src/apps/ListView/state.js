import * as R from 'ramda'
import {parseFilter} from '../../utilities/listview-helpers'
import G from './globals'


var app = {
  STATE: {
    searchParams: R.compose(
      R.map(R.split(',')),
      R.fromPairs,
      Array.from
    )((new URL(window.location)).searchParams.entries()),
    filters: R.compose(
      R.map(parseFilter),
      Array.from
    )(document.getElementById('MITHRIL-FILTERS-filters').children),
    pagination: G.pagination,
  },

  ACTIONS: {
    // Applies func to params, then goes to new
    // url using said params
    go: function(func){
      var x = R.join('', [
        '//',
        window.location.host,
        window.location.pathname,
        '?',
        new URLSearchParams(
          R.compose(
            R.toPairs,
            func
          )(app.STATE.searchParams)
        )
      ])
      window.location = x
    },

    initFilterValues: function(){
      app.STATE.filters = app.STATE.filters.map(
        function(filter){
          return R.set(
            R.lensProp('value'),
            R.prop(filter.filter, app.STATE.searchParams)
          )(filter)
        }
      )
    },

    updateFilter: R.curry(function(index, val){
      app.STATE.filters = R.set(
        R.lensPath([index, 'value']),
        val,
        app.STATE.filters
      )
    })
  }
}


export default app
