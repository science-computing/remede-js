import * as R from 'ramda'

const globals = {
  classes: R.mergeDeepRight({
    pagination: {
      div: '',
      item_content: '',
      link: '',
      list: '',
      text: '',
      select: '',
    },
    filters: {
      div: '',
      title: '',
      button_div: '',
      apply_button: '',
      clear_button: '',
      input_div: '',
      input: '',
      input_help: '',
      input_label: ''
    },
    table: {}
  }, window.CLASSES),
  pagination: window.PAGINATION,
  filtering:{
    orderby_param: 'order_by',
    page_param: 'page'
  },
}
  
export default globals
