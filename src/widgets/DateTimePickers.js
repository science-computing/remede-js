import {R} from '../remede'
import mm from '../utilities/millimount'

import {DateInput} from '../components/date-time-inputs'
import attrsToObj from '../utilities/attrsToObj'

var milliDateInput = R.curry(function(vn, update){
  var oninput = function(val){
    return update(R.mergeDeepLeft({date: val}))
  }
  var tick = function(){
    return update(R.identity)
  }

  return {
    model: function(){
      return {date: R.defaultTo('', vn.value)}
    },
    view: function(model){
      return m(
        DateInput,
        R.mergeDeepLeft(
          {
            oninput: oninput,
            tick: tick,
            value: model.date
          },
          vn
        )
      )
    }
  }
})


const remedeDatePicker = function(elements, config){
  var isNodeList = (
    typeof elements.length != 'undefined' &&
    typeof elements.item != 'undefined'
  )
  if (!isNodeList){
    elements = [elements]
  }
  for (var n=0; n < elements.length; n++){
    var vn = R.mergeDeepLeft(attrsToObj(elements[n]), config||{})
    var newElement = document.createElement('div')
    elements[n].parentNode.replaceChild(newElement, elements[n])
    mm(newElement, milliDateInput(vn))
  }
}

// SUPER EXPORTS
window.remedeDatePicker = remedeDatePicker
