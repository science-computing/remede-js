import {R} from '../remede'
import mm from '../utilities/millimount'

import DynamicSelectInput from '../components/dynamic-select'
import attrsToObj from '../utilities/attrsToObj'

var milliDynamicSelect = R.curry(function(vn, update){
  var oninput = function(val){
    return update(R.mergeDeepLeft({value: val}))
  }
  var tick = function(){
    return update(R.identity)
  }

  return {
    model: function(){
      return {value: R.defaultTo('', vn.value)}
    },
    view: function(model){
      return m(
        DynamicSelectInput,
        R.mergeDeepLeft(
          {
            oninput: oninput,
            tick: tick,
            value: model.value
          },
          vn
        )
      )
    }
  }
})


const remedeDynamicSelect = function(elements, config){
  var isNodeList = (
    typeof elements.length != 'undefined' &&
    typeof elements.item != 'undefined'
  )
  if (!isNodeList){
    elements = [elements]
  }
  for (var n=0; n < elements.length; n++){
    var vn = attrsToObj(elements[n])
    console.log(vn)
    var newElement = document.createElement('div')
    elements[n].parentNode.replaceChild(newElement, elements[n])
    mm(newElement, milliDynamicSelect(R.mergeDeepRight(vn, R.defaultTo({}, config))))
  }
}

// SUPER EXPORTS
window.remedeDynamicSelect = remedeDynamicSelect
