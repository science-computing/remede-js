const path = require('path')


module.exports = {
  entry: {
    ListView: './src/apps/ListView/app.js',
    DateTimePickers: './src/widgets/DateTimePickers.js',
    DynamicSelect: './src/widgets/DynamicSelect.js',
  },
  output: {
    path: path.resolve(__dirname, '../remede/static/remede/'),
    filename: '[name].js'
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        remede: {
          name: 'remede',
          chunks: 'initial',
          minChunks: 2
        }
      }
    }
  }
}
